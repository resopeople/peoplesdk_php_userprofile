<?php
/**
 * This class allows to define user profile attribute entity simple repository class.
 * User profile attribute entity simple repository is simple repository,
 * allows to prepare data from user profile attribute entity, to save in requisition persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\attribute\model\repository;

use people_sdk\library\model\repository\simple\model\SimpleRepository;

use liberty_code\model\repository\library\ConstRepository;
use people_sdk\user_profile\attribute\model\UserProfileAttrEntity;



class UserProfileAttrEntitySimpleRepository extends SimpleRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Init var
        $result = array(
            ConstRepository::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => UserProfileAttrEntity::class,
            ConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE => false
        );

        // Return result
        return $result;
    }



}


