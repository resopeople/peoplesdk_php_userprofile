<?php
/**
 * This class allows to define user profile attribute entity collection class.
 * User profile attribute entity collection is a save attribute entity collection,
 * used to store user profile attribute entities.
 * key: attribute key => user profile attribute entity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\attribute\model;

use liberty_code\attribute_model\attribute\repository\model\SaveAttributeEntityCollection;

use people_sdk\user_profile\attribute\model\UserProfileAttrEntity;



/**
 * @method null|UserProfileAttrEntity getItem(string $strKey) @inheritdoc
 * @method null|UserProfileAttrEntity getObjAttribute(string $strName) @inheritdoc
 * @method string setItem(UserProfileAttrEntity $objEntity) @inheritdoc
 * @method string setAttribute(UserProfileAttrEntity $objAttribute) @inheritdoc
 * @method UserProfileAttrEntity removeItem($strKey) @inheritdoc
 * @method UserProfileAttrEntity removeAttribute(string $strName) @inheritdoc
 */
class UserProfileAttrEntityCollection extends SaveAttributeEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return UserProfileAttrEntity::class;
    }



}