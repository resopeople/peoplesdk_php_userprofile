<?php
/**
 * Description :
 * This class allows to define user profile attribute provider class.
 * User profile attribute provider is standard attribute provider,
 * uses user profile attribute entity collection,
 * to provide attribute information, for user profile entity.
 *
 * User profile attribute provider uses the following specified configuration:
 * [
 *     Standard attribute provider configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\attribute\provider\model;

use liberty_code\handle_model\attribute\provider\standard\model\StandardAttrProvider;

use liberty_code\cache\repository\api\RepositoryInterface;
use people_sdk\user_profile\attribute\model\UserProfileAttrEntityCollection;



/**
 * @method UserProfileAttrEntityCollection getObjAttributeCollection() @inheritdoc
 * @method void setObjAttributeCollection(UserProfileAttrEntityCollection $objAttributeCollection) @inheritdoc
 */
class UserProfileAttrProvider extends StandardAttrProvider
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param UserProfileAttrEntityCollection $objAttributeCollection
     */
    public function __construct(
        UserProfileAttrEntityCollection $objAttributeCollection,
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objAttributeCollection,
            $tabConfig,
            $objCacheRepo
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     *
     * @return string
     */
    protected function getStrFixAttributeCollectionClassPath()
    {
        // Return result
        return UserProfileAttrEntityCollection::class;
    }



}