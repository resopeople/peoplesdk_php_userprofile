<?php
/**
 * This class allows to define user profile API key entity collection class.
 * key => UserProfileApiKeyEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\api\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\user_profile\api\model\UserProfileApiKeyEntity;



/**
 * @method null|UserProfileApiKeyEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(UserProfileApiKeyEntity $objEntity) @inheritdoc
 */
class UserProfileApiKeyEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return UserProfileApiKeyEntity::class;
    }



}