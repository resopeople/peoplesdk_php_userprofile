<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\api\requisition\request\info\library;

use liberty_code\library\instance\model\Multiton;

use people_sdk\library\requisition\request\info\library\ToolBoxSndInfo;
use people_sdk\user_profile\api\library\ConstUserProfileApiKey;
use people_sdk\user_profile\api\model\UserProfileApiKeyEntity;



class ToolBoxUserProfileApiKeySndInfo extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get sending information array,
     * with user API key authentication,
     * from specified user profile API key entity,
     * to connect to specific user profile, using HTTP API key format.
     *
     * Sending information array format:
     * @see ToolBoxSndInfo::getTabSndInfoWithAuthUserApiKey() sending information array format.
     *
     * Return format:
     * @see ToolBoxSndInfo::getTabSndInfoWithAuthUserApiKey() return format.
     *
     * @param UserProfileApiKeyEntity $objUserProfileApiKeyEntity
     * @param boolean $boolOnHeaderRequired = true
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithAuthUserApiKey(
        UserProfileApiKeyEntity $objUserProfileApiKeyEntity,
        $boolOnHeaderRequired = true,
        array $tabInfo = null
    )
    {
        // Check argument
        $objUserProfileApiKeyEntity->setAttributeValid(ConstUserProfileApiKey::ATTRIBUTE_KEY_KEY);

        // Return result
        return ToolBoxSndInfo::getTabSndInfoWithAuthUserApiKey(
            $objUserProfileApiKeyEntity->getAttributeValue(ConstUserProfileApiKey::ATTRIBUTE_KEY_KEY),
            $boolOnHeaderRequired,
            $tabInfo
        );
    }



}