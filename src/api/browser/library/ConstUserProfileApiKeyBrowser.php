<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\api\browser\library;



class ConstUserProfileApiKeyBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CRIT_EQUAL_ID = 'intAttrCritEqualId';
    const ATTRIBUTE_KEY_CRIT_IN_ID = 'tabAttrCritInId';
    const ATTRIBUTE_KEY_CRIT_START_DT_CREATE = 'attrCritStartDtCreate';
    const ATTRIBUTE_KEY_CRIT_END_DT_CREATE = 'attrCritEndDtCreate';
    const ATTRIBUTE_KEY_CRIT_START_DT_UPDATE = 'attrCritStartDtUpdate';
    const ATTRIBUTE_KEY_CRIT_END_DT_UPDATE = 'attrCritEndDtUpdate';
    const ATTRIBUTE_KEY_CRIT_LIKE_NAME = 'strAttrCritLikeName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_NAME = 'strAttrCritEqualName';
    const ATTRIBUTE_KEY_CRIT_IN_NAME = 'tabAttrCritInName';
    const ATTRIBUTE_KEY_CRIT_IS_BLOCKED = 'boolAttrCritIsBlocked';
    const ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID = 'intAttrCritEqualUserProfileId';
    const ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID = 'tabAttrCritInUserProfileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN = 'strAttrCritLikeUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN = 'strAttrCritEqualUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN = 'tabAttrCritInUserProfileLogin';
    const ATTRIBUTE_KEY_SORT_ID = 'strAttrSortId';
    const ATTRIBUTE_KEY_SORT_DT_CREATE = 'strAttrSortDtCreate';
    const ATTRIBUTE_KEY_SORT_DT_UPDATE = 'strAttrSortDtUpdate';
    const ATTRIBUTE_KEY_SORT_NAME = 'strAttrSortName';
    const ATTRIBUTE_KEY_SORT_USER_PROFILE_ID = 'strAttrSortUserProfileId';
    const ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN = 'strAttrSortUserProfileLogin';

    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ID = 'crit-equal-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_ID = 'crit-in-id';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE = 'crit-start-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE = 'crit-end-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE = 'crit-start-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE = 'crit-end-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_NAME = 'crit-like-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_NAME = 'crit-equal-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_NAME = 'crit-in-name';
    const ATTRIBUTE_ALIAS_CRIT_IS_BLOCKED = 'crit-is-blocked';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_ID = 'crit-equal-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_ID = 'crit-in-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_USER_PROFILE_LOGIN = 'crit-like-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_LOGIN = 'crit-equal-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_LOGIN = 'crit-in-profile-login';
    const ATTRIBUTE_ALIAS_SORT_ID = 'sort-id';
    const ATTRIBUTE_ALIAS_SORT_DT_CREATE = 'sort-dt-create';
    const ATTRIBUTE_ALIAS_SORT_DT_UPDATE = 'sort-dt-update';
    const ATTRIBUTE_ALIAS_SORT_NAME = 'sort-name';
    const ATTRIBUTE_ALIAS_SORT_USER_PROFILE_ID = 'sort-profile-id';
    const ATTRIBUTE_ALIAS_SORT_USER_PROFILE_LOGIN = 'sort-profile-login';



}