<?php
/**
 * This class allows to define user profile token key entity factory class.
 * User profile token key entity factory allows to provide new user profile token key entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\token\model;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\user_profile\user\model\UserProfileEntityFactory;
use people_sdk\user_profile\token\library\ConstUserProfileTokenKey;
use people_sdk\user_profile\token\exception\UserProfileEntityFactoryInvalidFormatException;
use people_sdk\user_profile\token\exception\UserProfileEntityFactoryExecConfigInvalidFormatException;
use people_sdk\user_profile\token\model\UserProfileTokenKeyEntity;



/**
 * @method null|UserProfileEntityFactory getObjUserProfileEntityFactory() Get user profile entity factory object.
 * @method null|array getTabUserProfileEntityFactoryExecConfig() Get user profile entity factory execution configuration array.
 * @method UserProfileTokenKeyEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method void setObjUserProfileEntityFactory(null|UserProfileEntityFactory $objUserProfileEntityFactory) Set user profile entity factory object.
 * @method void setTabUserProfileEntityFactoryExecConfig(null|array $tabUserProfileEntityFactoryExecConfig) Set user profile entity factory execution configuration array.
 */
class UserProfileTokenKeyEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|UserProfileEntityFactory $objUserProfileEntityFactory = null
     * @param null|array $tabUserProfileEntityFactoryExecConfig = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null,
        UserProfileEntityFactory $objUserProfileEntityFactory = null,
        array $tabUserProfileEntityFactoryExecConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objProvider,
            $objEntityCollection
        );

        // Init user profile entity factory
        $this->setObjUserProfileEntityFactory($objUserProfileEntityFactory);

        // Init user profile entity factory execution config
        $this->setTabUserProfileEntityFactoryExecConfig($tabUserProfileEntityFactoryExecConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstUserProfileTokenKey::DATA_KEY_USER_PROFILE_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstUserProfileTokenKey::DATA_KEY_USER_PROFILE_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstUserProfileTokenKey::DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstUserProfileTokenKey::DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstUserProfileTokenKey::DATA_KEY_USER_PROFILE_ENTITY_FACTORY,
            ConstUserProfileTokenKey::DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstUserProfileTokenKey::DATA_KEY_USER_PROFILE_ENTITY_FACTORY:
                    UserProfileEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstUserProfileTokenKey::DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG:
                    UserProfileEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => UserProfileTokenKeyEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => ConstUserProfileTokenKey::ATTRIBUTE_KEY_ID
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new UserProfileTokenKeyEntity(
            array(),
            $objValidator,
            $objDateTimeFactory,
            $this->getObjUserProfileEntityFactory(),
            $this->getTabUserProfileEntityFactoryExecConfig()
        );

        // Return result
        return $result;
    }



}