<?php
/**
 * This class allows to define user profile token key entity collection class.
 * key => UserProfileTokenKeyEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\token\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\user_profile\token\model\UserProfileTokenKeyEntity;



/**
 * @method null|UserProfileTokenKeyEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(UserProfileTokenKeyEntity $objEntity) @inheritdoc
 */
class UserProfileTokenKeyEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return UserProfileTokenKeyEntity::class;
    }



}