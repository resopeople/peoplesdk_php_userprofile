<?php
/**
 * This class allows to define user profile token key browser entity class.
 * User profile token key browser entity allows to define attributes,
 * to search user profile token key entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\token\browser\model;

use people_sdk\library\browser\model\DefaultBrowserEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\browser\library\ToolBoxValidBrowserEntity;
use people_sdk\user_profile\token\browser\library\ConstUserProfileTokenKeyBrowser;



/**
 * @property null|integer $intAttrCritEqualId
 * @property null|integer[] $tabAttrCritInId
 * @property null|string|DateTime $attrCritStartDtCreate
 * @property null|string|DateTime $attrCritEndDtCreate
 * @property null|string|DateTime $attrCritStartDtUpdate
 * @property null|string|DateTime $attrCritEndDtUpdate
 * @property null|string|DateTime $attrCritStartDtValidStart
 * @property null|string|DateTime $attrCritEndDtValidStart
 * @property null|string|DateTime $attrCritStartDtValidEnd
 * @property null|string|DateTime $attrCritEndDtValidEnd
 * @property null|string $strAttrCritLikeName
 * @property null|string $strAttrCritEqualName
 * @property null|string[] $tabAttrCritInName
 * @property null|integer $intAttrCritEqualUserProfileId
 * @property null|integer[] $tabAttrCritInUserProfileId
 * @property null|string $strAttrCritLikeUserProfileLogin
 * @property null|string $strAttrCritEqualUserProfileLogin
 * @property null|string[] $tabAttrCritInUserProfileLogin
 * @property null|string $strAttrSortId
 * @property null|string $strAttrSortDtCreate
 * @property null|string $strAttrSortDtUpdate
 * @property null|string $strAttrSortDtValidStart
 * @property null|string $strAttrSortDtValidEnd
 * @property null|string $strAttrSortName
 * @property null|string $strAttrSortUserProfileId
 * @property null|string $strAttrSortUserProfileLogin
 */
class UserProfileTokenKeyBrowserEntity extends DefaultBrowserEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array_merge(
            parent::getTabConfig(),
            array(
                // Attribute criteria equal id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],


                // Attribute criteria start datetime validity start
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_VALID_START,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_VALID_START,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime validity start
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_VALID_START,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_VALID_START,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime validity end
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_VALID_END,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_VALID_END,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime validity end
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_VALID_END,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_VALID_END,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_CRIT_IN_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime validity start
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_DT_VALID_START,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_SORT_DT_VALID_START,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime validity end
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_DT_VALID_END,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_SORT_DT_VALID_END,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_SORT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_SORT_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileTokenKeyBrowser::ATTRIBUTE_ALIAS_SORT_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-integer' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                ]
            ]
        );
        $tabRuleConfigValidTabId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-integer' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strict positive integers.'
                ]
            ]
        );
        $tabRuleConfigValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty']
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty'
                ]
            ]
        );
        $tabRuleConfigValidTabString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-string' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => ['is_empty']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strings not empty.'
                ]
            ]
        );
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidSort = ToolBoxValidBrowserEntity::getTabSortRuleConfig();

        // Init var
        $result = array_merge(
            parent::getTabRuleConfig(),
            array(
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID => $tabRuleConfigValidId,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_IN_ID => $tabRuleConfigValidTabId,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE => $tabRuleConfigValidDate,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE => $tabRuleConfigValidDate,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE => $tabRuleConfigValidDate,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE => $tabRuleConfigValidDate,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_VALID_START => $tabRuleConfigValidDate,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_VALID_START => $tabRuleConfigValidDate,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_VALID_END => $tabRuleConfigValidDate,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_VALID_END => $tabRuleConfigValidDate,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME => $tabRuleConfigValidString,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME => $tabRuleConfigValidString,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME => $tabRuleConfigValidTabString,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID => $tabRuleConfigValidId,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN => $tabRuleConfigValidTabString,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_ID => $tabRuleConfigValidSort,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE => $tabRuleConfigValidSort,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE => $tabRuleConfigValidSort,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_DT_VALID_START => $tabRuleConfigValidSort,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_DT_VALID_END => $tabRuleConfigValidSort,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_NAME => $tabRuleConfigValidSort,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_ID => $tabRuleConfigValidSort,
                ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN => $tabRuleConfigValidSort
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_VALID_START:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_VALID_START:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_VALID_END:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_VALID_END:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_IN_ID:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_array($value)) ?
                                array_values(array_map(
                                    function($value){
                                        return (
                                            (is_string($value) && ctype_digit($value)) ?
                                                intval($value) :
                                                $value
                                        );
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_VALID_START:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_VALID_START:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_VALID_END:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_VALID_END:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            ((!is_null($objDtFactory)) && is_string($value)) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_ID:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_DT_VALID_START:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_DT_VALID_END:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_NAME:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_ID:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        ((is_array($value)) ? array_values($value) : $value)
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getAttributeValueQueryFormat($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_VALID_START:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_VALID_START:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_START_DT_VALID_END:
            case ConstUserProfileTokenKeyBrowser::ATTRIBUTE_KEY_CRIT_END_DT_VALID_END:
                $result = $this->getAttributeValueFormatGet($strKey, $value);
                break;

            default:
                $result = parent::getAttributeValueQueryFormat($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}