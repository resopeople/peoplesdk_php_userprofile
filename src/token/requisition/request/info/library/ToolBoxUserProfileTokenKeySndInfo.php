<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\token\requisition\request\info\library;

use liberty_code\library\instance\model\Multiton;

use people_sdk\library\requisition\request\info\library\ToolBoxSndInfo;
use people_sdk\user_profile\token\library\ConstUserProfileTokenKey;
use people_sdk\user_profile\token\model\UserProfileTokenKeyEntity;



class ToolBoxUserProfileTokenKeySndInfo extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get sending information array,
     * with user bearer authentication,
     * from specified user profile token key entity,
     * to connect to specific user profile, using HTTP bearer format.
     *
     * Sending information array format:
     * @see ToolBoxSndInfo::getTabSndInfoWithAuthUserBearer() sending information array format.
     *
     * Return format:
     * @see ToolBoxSndInfo::getTabSndInfoWithAuthUserBearer() return format.
     *
     * @param UserProfileTokenKeyEntity $objUserProfileTokenKeyEntity
     * @param boolean $boolOnHeaderRequired = true
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithAuthUserBearer(
        UserProfileTokenKeyEntity $objUserProfileTokenKeyEntity,
        $boolOnHeaderRequired = true,
        array $tabInfo = null
    )
    {
        // Check argument
        $objUserProfileTokenKeyEntity->setAttributeValid(ConstUserProfileTokenKey::ATTRIBUTE_KEY_KEY);

        // Return result
        return ToolBoxSndInfo::getTabSndInfoWithAuthUserBearer(
            $objUserProfileTokenKeyEntity->getAttributeValue(ConstUserProfileTokenKey::ATTRIBUTE_KEY_KEY),
            $boolOnHeaderRequired,
            $tabInfo
        );
    }



}