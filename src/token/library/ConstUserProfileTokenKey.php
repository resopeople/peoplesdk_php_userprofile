<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\token\library;



class ConstUserProfileTokenKey
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_USER_PROFILE_ENTITY_FACTORY = 'objUserProfileEntityFactory';
    const DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG = 'tabUserProfileEntityFactoryExecConfig';



    // Attribute configuration
    const ATTRIBUTE_KEY_ID = 'intAttrId';
    const ATTRIBUTE_KEY_DT_CREATE = 'attrDtCreate';
    const ATTRIBUTE_KEY_DT_UPDATE = 'attrDtUpdate';
    const ATTRIBUTE_KEY_DT_VALID_START = 'attrDtValidStart';
    const ATTRIBUTE_KEY_DT_VALID_END = 'attrDtValidEnd';
    const ATTRIBUTE_KEY_NAME = 'strAttrName';
    const ATTRIBUTE_KEY_KEY = 'strAttrKey';
    const ATTRIBUTE_KEY_KEY_RESET = 'boolAttrKeyReset';
    const ATTRIBUTE_KEY_USER_PROFILE = 'attrUserProfile';

    const ATTRIBUTE_ALIAS_ID = 'id';
    const ATTRIBUTE_ALIAS_DT_CREATE = 'dt-create';
    const ATTRIBUTE_ALIAS_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_ALIAS_DT_VALID_START = 'dt-valid-start';
    const ATTRIBUTE_ALIAS_DT_VALID_END = 'dt-valid-end';
    const ATTRIBUTE_ALIAS_NAME = 'name';
    const ATTRIBUTE_ALIAS_KEY = 'key';
    const ATTRIBUTE_ALIAS_KEY_RESET = 'key-reset';
    const ATTRIBUTE_ALIAS_USER_PROFILE = 'user-profile';

    const ATTRIBUTE_NAME_SAVE_ID = 'id';
    const ATTRIBUTE_NAME_SAVE_DT_CREATE = 'dt-create';
    const ATTRIBUTE_NAME_SAVE_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_NAME_SAVE_DT_VALID_START = 'dt-valid-start';
    const ATTRIBUTE_NAME_SAVE_DT_VALID_END = 'dt-valid-end';
    const ATTRIBUTE_NAME_SAVE_NAME = 'name';
    const ATTRIBUTE_NAME_SAVE_KEY = 'key';
    const ATTRIBUTE_NAME_SAVE_KEY_RESET = 'key-reset';
    const ATTRIBUTE_NAME_SAVE_USER_PROFILE = 'profile';



    // Sub-action type configuration
    const SUB_ACTION_TYPE_ADMIN = 'admin';
    const SUB_ACTION_TYPE_PROFILE = 'profile';
    const SUB_ACTION_TYPE_PROFILE_CURRENT = 'profile_current';



    // Exception message constants
    const EXCEPT_MSG_USER_PROFILE_ENTITY_FACTORY_INVALID_FORMAT =
        'Following user profile entity factory "%1$s" invalid! It must be an user profile entity factory object.';
    const EXCEPT_MSG_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the user profile entity factory execution configuration standard.';



}