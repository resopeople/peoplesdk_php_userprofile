<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\user\requisition\request\info\library;

use liberty_code\library\instance\model\Multiton;

use people_sdk\library\requisition\request\info\library\ToolBoxSndInfo;
use people_sdk\user_profile\user\library\ConstUserProfile;
use people_sdk\user_profile\user\model\UserProfileEntity;



class ToolBoxUserProfileSndInfo extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get sending information array,
     * with user basic authentication,
     * from specified user profile entity,
     * to connect to specific user profile, using HTTP basic format.
     *
     * Sending information array format:
     * @see ToolBoxSndInfo::getTabSndInfoWithAuthUserBasic() sending information array format.
     *
     * Return format:
     * @see ToolBoxSndInfo::getTabSndInfoWithAuthUserBasic() return format.
     *
     * @param UserProfileEntity $objUserProfileEntity
     * @param boolean $boolOnHeaderRequired = true
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithAuthUserBasic(
        UserProfileEntity $objUserProfileEntity,
        $boolOnHeaderRequired = true,
        array $tabInfo = null
    )
    {
        // Check arguments
        $objUserProfileEntity->setAttributeValid(ConstUserProfile::ATTRIBUTE_KEY_LOGIN);
        $objUserProfileEntity->setAttributeValid(ConstUserProfile::ATTRIBUTE_KEY_PASSWORD);

        // Return result
        return ToolBoxSndInfo::getTabSndInfoWithAuthUserBasic(
            $objUserProfileEntity->getAttributeValue(ConstUserProfile::ATTRIBUTE_KEY_LOGIN),
            $objUserProfileEntity->getAttributeValue(ConstUserProfile::ATTRIBUTE_KEY_PASSWORD),
            $boolOnHeaderRequired,
            $tabInfo
        );
    }



}