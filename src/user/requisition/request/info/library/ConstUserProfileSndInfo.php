<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\user\requisition\request\info\library;



class ConstUserProfileSndInfo
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Headers configuration
    const HEADER_KEY_CURRENT_PROFILE_INCLUDE = 'Current-Profile-Include';
    const HEADER_KEY_PERM_FULL_UPDATE = 'Permission-Full-Update';
    const HEADER_KEY_ROLE_FULL_UPDATE = 'Role-Full-Update';



    // URL arguments configuration
    const URL_ARG_KEY_CURRENT_PROFILE_INCLUDE = 'current-profile-include';
    const URL_ARG_KEY_PERM_FULL_UPDATE = 'permission-full-update';
    const URL_ARG_KEY_ROLE_FULL_UPDATE = 'role-full-update';



}