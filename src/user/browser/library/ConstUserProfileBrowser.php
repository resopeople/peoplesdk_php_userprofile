<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\user\browser\library;



class ConstUserProfileBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CRIT_EQUAL_ID = 'intAttrCritEqualId';
    const ATTRIBUTE_KEY_CRIT_IN_ID = 'tabAttrCritInId';
    const ATTRIBUTE_KEY_CRIT_START_DT_CREATE = 'attrCritStartDtCreate';
    const ATTRIBUTE_KEY_CRIT_END_DT_CREATE = 'attrCritEndDtCreate';
    const ATTRIBUTE_KEY_CRIT_START_DT_UPDATE = 'attrCritStartDtUpdate';
    const ATTRIBUTE_KEY_CRIT_END_DT_UPDATE = 'attrCritEndDtUpdate';
    const ATTRIBUTE_KEY_CRIT_LIKE_LOGIN = 'strAttrCritLikeLogin';
    const ATTRIBUTE_KEY_CRIT_EQUAL_LOGIN = 'strAttrCritEqualLogin';
    const ATTRIBUTE_KEY_CRIT_IN_LOGIN = 'tabAttrCritInLogin';
    const ATTRIBUTE_KEY_CRIT_LIKE_EMAIL = 'strAttrCritLikeEmail';
    const ATTRIBUTE_KEY_CRIT_EQUAL_EMAIL = 'strAttrCritEqualEmail';
    const ATTRIBUTE_KEY_CRIT_IN_EMAIL = 'tabAttrCritInEmail';
    const ATTRIBUTE_KEY_CRIT_IS_BLOCKED = 'boolAttrCritIsBlocked';
    const ATTRIBUTE_KEY_SORT_ID = 'strAttrSortId';
    const ATTRIBUTE_KEY_SORT_DT_CREATE = 'strAttrSortDtCreate';
    const ATTRIBUTE_KEY_SORT_DT_UPDATE = 'strAttrSortDtUpdate';
    const ATTRIBUTE_KEY_SORT_LOGIN = 'strAttrSortLogin';
    const ATTRIBUTE_KEY_SORT_EMAIL = 'strAttrSortEmail';

    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ID = 'crit-equal-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_ID = 'crit-in-id';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE = 'crit-start-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE = 'crit-end-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE = 'crit-start-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE = 'crit-end-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_LOGIN = 'crit-like-login';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_LOGIN = 'crit-equal-login';
    const ATTRIBUTE_ALIAS_CRIT_IN_LOGIN = 'crit-in-login';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_EMAIL = 'crit-like-email';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_EMAIL = 'crit-equal-email';
    const ATTRIBUTE_ALIAS_CRIT_IN_EMAIL = 'crit-in-email';
    const ATTRIBUTE_ALIAS_CRIT_IS_BLOCKED = 'crit-is-blocked';
    const ATTRIBUTE_ALIAS_SORT_ID = 'sort-id';
    const ATTRIBUTE_ALIAS_SORT_DT_CREATE = 'sort-dt-create';
    const ATTRIBUTE_ALIAS_SORT_DT_UPDATE = 'sort-dt-update';
    const ATTRIBUTE_ALIAS_SORT_LOGIN = 'sort-login';
    const ATTRIBUTE_ALIAS_SORT_EMAIL = 'sort-email';



}