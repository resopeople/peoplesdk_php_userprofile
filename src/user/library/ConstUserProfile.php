<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\user\library;



class ConstUserProfile
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_USER_PROFILE_ATTR_VALUE_ENTITY_FACTORY = 'objUserProfileAttrValueEntityFactory';
    const DATA_KEY_USER_PROFILE_ATTR_VALUE_ENTITY_FACTORY_EXEC_CONFIG = 'tabUserProfileAttrValueEntityFactoryExecConfig';



    // Attribute configuration
    const ATTRIBUTE_KEY_ID = 'intAttrId';
    const ATTRIBUTE_KEY_DT_CREATE = 'attrDtCreate';
    const ATTRIBUTE_KEY_DT_UPDATE = 'attrDtUpdate';
    const ATTRIBUTE_KEY_LOGIN = 'strAttrLogin';
    const ATTRIBUTE_KEY_PASSWORD = 'strAttrPassword';
    const ATTRIBUTE_KEY_PASSWORD_RESET = 'boolAttrPasswordReset';
    const ATTRIBUTE_KEY_PASSWORD_VERIFY = 'strAttrPasswordVerify';
    const ATTRIBUTE_KEY_PASSWORD_NEW = 'strAttrPasswordNew';
    const ATTRIBUTE_KEY_EMAIL = 'strAttrEmail';
    const ATTRIBUTE_KEY_IS_BLOCKED = 'boolAttrIsBlocked';
    const ATTRIBUTE_KEY_IMG_SRC_NAME = 'strAttrImgSrcName';
    const ATTRIBUTE_KEY_IMG_TYPE = 'strAttrImgType';
    const ATTRIBUTE_KEY_IMG_SIZE = 'intAttrImgSize';
    const ATTRIBUTE_KEY_IMAGE = 'attrImage';
    const ATTRIBUTE_KEY_USER_PROFILE_ATTR_VALUE_ENTITY = 'objAttrUserProfileAttrValueEntity';

    const ATTRIBUTE_ALIAS_ID = 'id';
    const ATTRIBUTE_ALIAS_DT_CREATE = 'dt-create';
    const ATTRIBUTE_ALIAS_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_ALIAS_LOGIN = 'login';
    const ATTRIBUTE_ALIAS_PASSWORD = 'password';
    const ATTRIBUTE_ALIAS_PASSWORD_RESET = 'password-reset';
    const ATTRIBUTE_ALIAS_PASSWORD_VERIFY = 'password-verify';
    const ATTRIBUTE_ALIAS_PASSWORD_NEW = 'password-new';
    const ATTRIBUTE_ALIAS_EMAIL = 'email';
    const ATTRIBUTE_ALIAS_IS_BLOCKED = 'is-blocked';
    const ATTRIBUTE_ALIAS_IMG_SRC_NAME = 'image-source-name';
    const ATTRIBUTE_ALIAS_IMG_TYPE = 'image-type';
    const ATTRIBUTE_ALIAS_IMG_SIZE = 'image-size';
    const ATTRIBUTE_ALIAS_IMAGE = 'image';
    const ATTRIBUTE_ALIAS_USER_PROFILE_ATTR_VALUE_ENTITY = 'attribute';

    const ATTRIBUTE_NAME_SAVE_ID = 'id';
    const ATTRIBUTE_NAME_SAVE_DT_CREATE = 'dt-create';
    const ATTRIBUTE_NAME_SAVE_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_NAME_SAVE_LOGIN = 'login';
    const ATTRIBUTE_NAME_SAVE_PASSWORD = 'password';
    const ATTRIBUTE_NAME_SAVE_PASSWORD_RESET = 'password-reset';
    const ATTRIBUTE_NAME_SAVE_PASSWORD_VERIFY = 'password-verify';
    const ATTRIBUTE_NAME_SAVE_PASSWORD_NEW = 'password-new';
    const ATTRIBUTE_NAME_SAVE_EMAIL = 'email';
    const ATTRIBUTE_NAME_SAVE_IS_BLOCKED = 'is-blocked';
    const ATTRIBUTE_NAME_SAVE_IMG_SRC_NAME = 'image-source-name';
    const ATTRIBUTE_NAME_SAVE_IMG_TYPE = 'image-type';
    const ATTRIBUTE_NAME_SAVE_IMG_SIZE = 'image-size';
    const ATTRIBUTE_NAME_SAVE_IMAGE = 'image';
    const ATTRIBUTE_NAME_SAVE_USER_PROFILE_ATTR_VALUE_ENTITY = 'attribute';



    // Sub-action type configuration
    const SUB_ACTION_TYPE_ADMIN = 'admin';
    const SUB_ACTION_TYPE_ADMIN_ROLE = 'admin_role';
    const SUB_ACTION_TYPE_PROFILE = 'profile';
    const SUB_ACTION_TYPE_PROFILE_ROLE = 'profile_role';
    const SUB_ACTION_TYPE_PROFILE_USER_PROFILE = 'profile_user_profile';



    // Exception message constants
    const EXCEPT_MSG_USER_PROFILE_ATTR_VALUE_ENTITY_INVALID_FORMAT =
        'Following user profile attribute value entity factory "%1$s" invalid! It must be an user profile attribute value entity factory object.';
    const EXCEPT_MSG_USER_PROFILE_ATTR_VALUE_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the user profile attribute value entity factory execution configuration standard.';



}