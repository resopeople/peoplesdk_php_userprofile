<?php
/**
 * This class allows to define user profile entity class.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\user\model;

use people_sdk\role\profile\model\RoleProfileEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\file\file\api\FileInterface;
use liberty_code\file\file\name\api\NameFileInterface;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\null_value\library\ConstNullValue;
use people_sdk\library\model\entity\null_value\library\ToolBoxNullValue;
use people_sdk\role\permission\subject\model\SubjPermEntityFactory;
use people_sdk\role\role\model\RoleEntityFactory;
use people_sdk\user_profile\user\attribute\value\model\UserProfileAttrValueEntity;
use people_sdk\user_profile\user\attribute\value\model\UserProfileAttrValueEntityFactory;
use people_sdk\user_profile\user\library\ConstUserProfile;



/**
 * @property null|integer $intAttrId
 * @property null|string|DateTime $attrDtCreate
 * @property null|string|DateTime $attrDtUpdate
 * @property null|string $strAttrLogin
 * @property null|string $strAttrPassword
 * @property null|boolean $boolAttrPasswordReset
 * @property null|string $strAttrPasswordVerify
 * @property null|string $strAttrPasswordNew
 * @property null|string $strAttrEmail
 * @property null|boolean $boolAttrIsBlocked
 * @property null|string $strAttrImgSrcName
 * @property null|string $strAttrImgType : image mime type
 * @property null|integer $intAttrImgSize : image size in bytes
 * @property null|string|FileInterface $attrImage : image URL or file
 * @property null|UserProfileAttrValueEntity $objAttrUserProfileAttrValueEntity
 */
class UserProfileEntity extends RoleProfileEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;



    /**
     * DI: User profile attribute value entity factory instance.
     * @var null|UserProfileAttrValueEntityFactory
     */
    protected $objUserProfileAttrValueEntityFactory;



    /**
     * DI: User profile attribute value entity factory execution configuration.
     * @var null|array
     */
    protected $tabUserProfileAttrValueEntityFactoryExecConfig;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|DateTimeFactoryInterface $objDateTimeFactory = null
     * @param null|UserProfileAttrValueEntityFactory $objUserProfileAttrValueEntityFactory = null
     * @param null|array $tabUserProfileAttrValueEntityFactoryExecConfig = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null,
        UserProfileAttrValueEntityFactory $objUserProfileAttrValueEntityFactory = null,
        SubjPermEntityFactory $objSubjPermEntityFactory = null,
        RoleEntityFactory $objRoleEntityFactory = null,
        array $tabUserProfileAttrValueEntityFactoryExecConfig = null,
        array $tabSubjPermEntityFactoryExecConfig = null,
        array $tabRoleEntityFactoryExecConfig = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;
        $this->objUserProfileAttrValueEntityFactory = $objUserProfileAttrValueEntityFactory;
        $this->tabUserProfileAttrValueEntityFactoryExecConfig = $tabUserProfileAttrValueEntityFactoryExecConfig;

        // Call parent constructor
        parent::__construct(
            $tabValue,
            $objValidator,
            $objSubjPermEntityFactory,
            $objRoleEntityFactory,
            $tabSubjPermEntityFactoryExecConfig,
            $tabRoleEntityFactoryExecConfig
        );
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkAttributeSaveGetEnabled($strKey)
    {
        // Init var
        $value = $this->getAttributeValue($strKey, false);

        // Check by attribute
        switch($strKey)
        {
            case ConstUserProfile::ATTRIBUTE_KEY_IMAGE:
                $result = (
                    parent::checkAttributeSaveGetEnabled($strKey) &&
                    (
                        (!is_string($value)) ||
                        ($value === ConstNullValue::NULL_VALUE)
                    )
                );
                break;

            default:
                $result = parent::checkAttributeSaveGetEnabled($strKey);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getStrBaseKey()
    {
        // Init var
        $intId = $this->getAttributeValue(ConstUserProfile::ATTRIBUTE_KEY_ID);
        $result = (
            (is_int($intId) && ($intId > 0)) ?
                strval($intId) :
                parent::getStrBaseKey()
        );

        // Return result
        return $result;
    }



    /**
     * Get default attribute user profile attribute value entity,
     * used on attribute configuration.
     *
     * @return null|UserProfileAttrValueEntity
     */
    protected function getObjDefaultAttrUserProfileAttrValueEntity()
    {
        // Return result
        return (
            (!is_null($this->objUserProfileAttrValueEntityFactory)) ?
                $this
                    ->objUserProfileAttrValueEntityFactory
                    ->getObjEntity(
                        array(),
                        $this->tabUserProfileAttrValueEntityFactoryExecConfig
                    ) :
                null
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array_merge(
            parent::getTabConfig(),
            array(
                // Attribute id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfile::ATTRIBUTE_KEY_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfile::ATTRIBUTE_ALIAS_ID,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfile::ATTRIBUTE_KEY_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfile::ATTRIBUTE_ALIAS_DT_CREATE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstUserProfile::ATTRIBUTE_NAME_SAVE_DT_CREATE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfile::ATTRIBUTE_KEY_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfile::ATTRIBUTE_ALIAS_DT_UPDATE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstUserProfile::ATTRIBUTE_NAME_SAVE_DT_UPDATE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfile::ATTRIBUTE_KEY_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfile::ATTRIBUTE_ALIAS_LOGIN,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstUserProfile::ATTRIBUTE_NAME_SAVE_LOGIN,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute password
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfile::ATTRIBUTE_KEY_PASSWORD,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfile::ATTRIBUTE_ALIAS_PASSWORD,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstUserProfile::ATTRIBUTE_NAME_SAVE_PASSWORD,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute password reset
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfile::ATTRIBUTE_KEY_PASSWORD_RESET,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfile::ATTRIBUTE_ALIAS_PASSWORD_RESET,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstUserProfile::ATTRIBUTE_NAME_SAVE_PASSWORD_RESET,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => false,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute password verify
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfile::ATTRIBUTE_KEY_PASSWORD_VERIFY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfile::ATTRIBUTE_ALIAS_PASSWORD_VERIFY,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstUserProfile::ATTRIBUTE_NAME_SAVE_PASSWORD_VERIFY,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => false,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute password new
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfile::ATTRIBUTE_KEY_PASSWORD_NEW,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfile::ATTRIBUTE_ALIAS_PASSWORD_NEW,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstUserProfile::ATTRIBUTE_NAME_SAVE_PASSWORD_NEW,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => false,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfile::ATTRIBUTE_KEY_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfile::ATTRIBUTE_ALIAS_EMAIL,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstUserProfile::ATTRIBUTE_NAME_SAVE_EMAIL,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute is blocked
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfile::ATTRIBUTE_KEY_IS_BLOCKED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfile::ATTRIBUTE_ALIAS_IS_BLOCKED,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstUserProfile::ATTRIBUTE_NAME_SAVE_IS_BLOCKED,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute image source name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfile::ATTRIBUTE_KEY_IMG_SRC_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfile::ATTRIBUTE_ALIAS_IMG_SRC_NAME,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstUserProfile::ATTRIBUTE_NAME_SAVE_IMG_SRC_NAME,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute image type
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfile::ATTRIBUTE_KEY_IMG_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfile::ATTRIBUTE_ALIAS_IMG_TYPE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstUserProfile::ATTRIBUTE_NAME_SAVE_IMG_TYPE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute image size
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfile::ATTRIBUTE_KEY_IMG_SIZE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfile::ATTRIBUTE_ALIAS_IMG_SIZE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstUserProfile::ATTRIBUTE_NAME_SAVE_IMG_SIZE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute image
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfile::ATTRIBUTE_KEY_IMAGE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfile::ATTRIBUTE_ALIAS_IMAGE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstUserProfile::ATTRIBUTE_NAME_SAVE_IMAGE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute user profile attribute value entity
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfile::ATTRIBUTE_KEY_USER_PROFILE_ATTR_VALUE_ENTITY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfile::ATTRIBUTE_ALIAS_USER_PROFILE_ATTR_VALUE_ENTITY,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstUserProfile::ATTRIBUTE_NAME_SAVE_USER_PROFILE_ATTR_VALUE_ENTITY,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE =>
                        $this->getObjDefaultAttrUserProfileAttrValueEntity()
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $getTabRuleConfigValidString = function($boolValueNullRequire = false)
        {
            $boolValueNullRequire = (is_bool($boolValueNullRequire) ? $boolValueNullRequire : true);
            $result = array(
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-string' => [
                                'type_string',
                                [
                                    'sub_rule_not',
                                    [
                                        'rule_config' => ['is_empty']
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a string not empty'
                    ]
                ]
            );

            if($boolValueNullRequire)
            {
                $result[0][1]['rule_config']['is-valid-null'] = array(
                    [
                        'compare_equal',
                        ['compare_value' => ConstNullValue::NULL_VALUE]
                    ]
                );
            }

            return $result;
        };
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );

        // Init var
        $result = parent::getTabRuleConfig();
        $result = array_merge(
            $result,
            array(
                ConstUserProfile::ATTRIBUTE_KEY_ID => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => [
                                    'is_null',
                                    [
                                        'callable',
                                        [
                                            'valid_callable' => function() {return $this->checkIsNew();}
                                        ]
                                    ]
                                ],
                                'is-valid-id' => [
                                    [
                                        'type_numeric',
                                        ['integer_only_require' => true]
                                    ],
                                    [
                                        'compare_greater',
                                        [
                                            'compare_value' => 0,
                                            'equal_enable_require' => false
                                        ]
                                    ],
                                    [
                                        'callable',
                                        [
                                            'valid_callable' => function() {return (!$this->checkIsNew());}
                                        ]
                                    ]
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                        ]
                    ]
                ],
                ConstUserProfile::ATTRIBUTE_KEY_DT_CREATE => $tabRuleConfigValidDate,
                ConstUserProfile::ATTRIBUTE_KEY_DT_UPDATE => $tabRuleConfigValidDate,
                ConstUserProfile::ATTRIBUTE_KEY_LOGIN => $getTabRuleConfigValidString(),
                ConstUserProfile::ATTRIBUTE_KEY_PASSWORD => $getTabRuleConfigValidString(),
                ConstUserProfile::ATTRIBUTE_KEY_PASSWORD_RESET => $tabRuleConfigValidBoolean,
                ConstUserProfile::ATTRIBUTE_KEY_PASSWORD_VERIFY => $getTabRuleConfigValidString(),
                ConstUserProfile::ATTRIBUTE_KEY_PASSWORD_NEW => $getTabRuleConfigValidString(),
                ConstUserProfile::ATTRIBUTE_KEY_EMAIL => $getTabRuleConfigValidString(),
                ConstUserProfile::ATTRIBUTE_KEY_IS_BLOCKED => $tabRuleConfigValidBoolean,
                ConstUserProfile::ATTRIBUTE_KEY_IMG_SRC_NAME => $getTabRuleConfigValidString(true),
                ConstUserProfile::ATTRIBUTE_KEY_IMG_TYPE => $getTabRuleConfigValidString(true),
                ConstUserProfile::ATTRIBUTE_KEY_IMG_SIZE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-null' => [
                                    [
                                        'compare_equal',
                                        ['compare_value' => ConstNullValue::NULL_VALUE]
                                    ]
                                ],
                                'is-valid-integer' => [
                                    [
                                        'type_numeric',
                                        ['integer_only_require' => true]
                                    ],
                                    [
                                        'compare_greater',
                                        [
                                            'compare_value' => 0,
                                            'equal_enable_require' => true
                                        ]
                                    ]
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be null or a positive integer.'
                        ]
                    ]
                ],
                ConstUserProfile::ATTRIBUTE_KEY_IMAGE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-null' => [
                                    [
                                        'compare_equal',
                                        ['compare_value' => ConstNullValue::NULL_VALUE]
                                    ]
                                ],
                                'is-valid-string' => [
                                    'type_string',
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => ['is_empty']
                                        ]
                                    ]
                                ],
                                'is-valid-file' => [
                                    [
                                        'type_object',
                                        [
                                            'class_path' => [FileInterface::class]
                                        ]
                                    ]
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be null, a string not empty or a valid file.'
                        ]
                    ]
                ],
                ConstUserProfile::ATTRIBUTE_KEY_USER_PROFILE_ATTR_VALUE_ENTITY => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-entity' => [
                                    [
                                        'type_object',
                                        [
                                            'class_path' => [UserProfileAttrValueEntity::class]
                                        ]
                                    ],
                                    ['validation_entity']
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be a valid user profile attribute value entity.'
                        ]
                    ]
                ]
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstUserProfile::ATTRIBUTE_KEY_DT_CREATE:
            case ConstUserProfile::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstUserProfile::ATTRIBUTE_KEY_ID:
            case ConstUserProfile::ATTRIBUTE_KEY_IMG_SIZE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstUserProfile::ATTRIBUTE_KEY_DT_CREATE:
            case ConstUserProfile::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            ((!is_null($objDtFactory)) && is_string($value)) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstUserProfile::ATTRIBUTE_KEY_LOGIN:
            case ConstUserProfile::ATTRIBUTE_KEY_PASSWORD:
            case ConstUserProfile::ATTRIBUTE_KEY_PASSWORD_VERIFY:
            case ConstUserProfile::ATTRIBUTE_KEY_PASSWORD_NEW:
            case ConstUserProfile::ATTRIBUTE_KEY_EMAIL:
            case ConstUserProfile::ATTRIBUTE_KEY_IMG_SRC_NAME:
            case ConstUserProfile::ATTRIBUTE_KEY_IMG_TYPE:
            case ConstUserProfile::ATTRIBUTE_KEY_IMAGE:
            case ConstUserProfile::ATTRIBUTE_KEY_USER_PROFILE_ATTR_VALUE_ENTITY:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstUserProfile::ATTRIBUTE_KEY_PASSWORD_RESET:
            case ConstUserProfile::ATTRIBUTE_KEY_IS_BLOCKED:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstUserProfile::ATTRIBUTE_KEY_DT_CREATE:
            case ConstUserProfile::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrSaveDt($value) :
                        $value
                );
                break;

            case ConstUserProfile::ATTRIBUTE_KEY_IMG_SRC_NAME:
            case ConstUserProfile::ATTRIBUTE_KEY_IMG_TYPE:
            case ConstUserProfile::ATTRIBUTE_KEY_IMG_SIZE:
                $result = ToolBoxNullValue::getAttributeValueSaveFormatGet($value);
                break;

            case ConstUserProfile::ATTRIBUTE_KEY_IMAGE:
                $result = (
                    ($value instanceof FileInterface) ?
                        (
                            ($value instanceof NameFileInterface) ?
                                $value :
                                base64_encode($value->getStrContent())
                        ) :
                        ToolBoxNullValue::getAttributeValueSaveFormatGet($value)
                );
                break;

            case ConstUserProfile::ATTRIBUTE_KEY_USER_PROFILE_ATTR_VALUE_ENTITY:
                $result = (
                    ($value instanceof UserProfileAttrValueEntity) ?
                        $value->getTabDataSave() :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;
        $objUserProfileAttrValueEntityFactory = $this->objUserProfileAttrValueEntityFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstUserProfile::ATTRIBUTE_KEY_DT_CREATE:
            case ConstUserProfile::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && is_string($value)) ?
                        $objDtFactory->getObjDtFromSave($value, true) :
                        $value
                );
                break;

            case ConstUserProfile::ATTRIBUTE_KEY_IMG_SRC_NAME:
            case ConstUserProfile::ATTRIBUTE_KEY_IMG_TYPE:
            case ConstUserProfile::ATTRIBUTE_KEY_IMG_SIZE:
            case ConstUserProfile::ATTRIBUTE_KEY_IMAGE:
                $result = ToolBoxNullValue::getAttributeValueSaveFormatSet($value);
                break;

            case ConstUserProfile::ATTRIBUTE_KEY_USER_PROFILE_ATTR_VALUE_ENTITY:
                $result = $value;
                if((!is_null($objUserProfileAttrValueEntityFactory)) && is_array($value))
                {
                    $objUserProfileAttrValueEntity = $objUserProfileAttrValueEntityFactory->getObjEntity(
                        array(),
                        $this->tabUserProfileAttrValueEntityFactoryExecConfig
                    );

                    // Get data (filter required due to potential different schema)
                    $tabIncludeKey = array_map(
                        function($strKey) use ($objUserProfileAttrValueEntity) {
                            return $objUserProfileAttrValueEntity->getAttributeNameSave($strKey);
                        },
                        $objUserProfileAttrValueEntity->getTabAttributeKey()
                    );
                    $tabData = array_filter(
                        $value,
                        function($key) use ($tabIncludeKey) {
                            return (
                                (!is_string($key)) ||
                                in_array($key, $tabIncludeKey)
                            );
                        },
                        ARRAY_FILTER_USE_KEY
                    );

                    // Hydrate user profile attribute value entity, from data
                    if($objUserProfileAttrValueEntity->hydrateSave($tabData))
                    {
                        $objUserProfileAttrValueEntity->setIsNew(false);
                        $result = $objUserProfileAttrValueEntity;
                    }
                }
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}