<?php
/**
 * This class allows to define user profile entity factory class.
 * User profile entity factory allows to provide new user profile entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\user\model;

use people_sdk\role\profile\model\RoleProfileEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\role\permission\subject\model\SubjPermEntityFactory;
use people_sdk\role\role\model\RoleEntityFactory;
use people_sdk\user_profile\user\attribute\value\model\UserProfileAttrValueEntityFactory;
use people_sdk\user_profile\user\library\ConstUserProfile;
use people_sdk\user_profile\user\exception\UserProfileAttrValueEntityFactoryInvalidFormatException;
use people_sdk\user_profile\user\exception\UserProfileAttrValueEntityFactoryExecConfigInvalidFormatException;
use people_sdk\user_profile\user\model\UserProfileEntity;



/**
 * @method null|UserProfileAttrValueEntityFactory getObjUserProfileAttrValueEntityFactory() Get user profile attribute value entity factory object.
 * @method null|array getTabUserProfileAttrValueEntityFactoryExecConfig() Get user profile attribute value entity factory execution configuration array.
 * @method UserProfileEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method void setObjUserProfileAttrValueEntityFactory(null|UserProfileAttrValueEntityFactory $objUserProfileAttrValueEntityFactory) Set user profile attribute value entity factory object.
 * @method void setTabUserProfileAttrValueEntityFactoryExecConfig(null|array $tabUserProfileAttrValueEntityFactoryExecConfig) Set user profile attribute value entity factory execution configuration array.
 */
class UserProfileEntityFactory extends RoleProfileEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|UserProfileAttrValueEntityFactory $objUserProfileAttrValueEntityFactory = null
     * @param null|array $tabUserProfileAttrValueEntityFactoryExecConfig = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null,
        UserProfileAttrValueEntityFactory $objUserProfileAttrValueEntityFactory = null,
        array $tabUserProfileAttrValueEntityFactoryExecConfig = null,
        array $tabSubjPermEntityFactoryExecConfig = null,
        array $tabRoleEntityFactoryExecConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objProvider,
            $objEntityCollection,
            $tabSubjPermEntityFactoryExecConfig,
            $tabRoleEntityFactoryExecConfig
        );

        // Init user profile attribute value entity factory
        $this->setObjUserProfileAttrValueEntityFactory($objUserProfileAttrValueEntityFactory);

        // Init user profile attribute value entity factory execution config
        $this->setTabUserProfileAttrValueEntityFactoryExecConfig($tabUserProfileAttrValueEntityFactoryExecConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstUserProfile::DATA_KEY_USER_PROFILE_ATTR_VALUE_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstUserProfile::DATA_KEY_USER_PROFILE_ATTR_VALUE_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstUserProfile::DATA_KEY_USER_PROFILE_ATTR_VALUE_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstUserProfile::DATA_KEY_USER_PROFILE_ATTR_VALUE_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstUserProfile::DATA_KEY_USER_PROFILE_ATTR_VALUE_ENTITY_FACTORY,
            ConstUserProfile::DATA_KEY_USER_PROFILE_ATTR_VALUE_ENTITY_FACTORY_EXEC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstUserProfile::DATA_KEY_USER_PROFILE_ATTR_VALUE_ENTITY_FACTORY:
                    UserProfileAttrValueEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstUserProfile::DATA_KEY_USER_PROFILE_ATTR_VALUE_ENTITY_FACTORY_EXEC_CONFIG:
                    UserProfileAttrValueEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => UserProfileEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => ConstUserProfile::ATTRIBUTE_KEY_ID
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $objSubjPermEntityFactory = $this->getObjInstance(SubjPermEntityFactory::class);
        $objRoleEntityFactory = $this->getObjInstance(RoleEntityFactory::class);
        $result = new UserProfileEntity(
            array(),
            $objValidator,
            $objDateTimeFactory,
            $this->getObjUserProfileAttrValueEntityFactory(),
            $objSubjPermEntityFactory,
            $objRoleEntityFactory,
            $this->getTabUserProfileAttrValueEntityFactoryExecConfig(),
            $this->getTabSubjPermEntityFactoryExecConfig(),
            $this->getTabRoleEntityFactoryExecConfig()
        );

        // Return result
        return $result;
    }



}