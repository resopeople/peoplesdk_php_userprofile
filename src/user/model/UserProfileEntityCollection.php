<?php
/**
 * This class allows to define user profile entity collection class.
 * key => UserProfileEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\user\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\user_profile\user\model\UserProfileEntity;



/**
 * @method null|UserProfileEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(UserProfileEntity $objEntity) @inheritdoc
 */
class UserProfileEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return UserProfileEntity::class;
    }



}