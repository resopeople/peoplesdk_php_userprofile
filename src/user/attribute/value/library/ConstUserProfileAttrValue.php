<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\user\attribute\value\library;



class ConstUserProfileAttrValue
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_USER_PROFILE_ATTR_PROVIDER = 'objUserProfileAttrProvider';



    // Exception message constants
    const EXCEPT_MSG_USER_PROFILE_ATTR_PROVIDER_INVALID_FORMAT =
        'Following user profile attribute provider "%1$s" invalid! It must be an user profile attribute provider object.';



}