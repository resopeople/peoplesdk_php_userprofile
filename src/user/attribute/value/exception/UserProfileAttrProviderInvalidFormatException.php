<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\user\attribute\value\exception;

use Exception;
use people_sdk\user_profile\attribute\provider\model\UserProfileAttrProvider;
use people_sdk\user_profile\user\attribute\value\library\ConstUserProfileAttrValue;



class UserProfileAttrProviderInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $provider
     */
	public function __construct($provider)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstUserProfileAttrValue::EXCEPT_MSG_USER_PROFILE_ATTR_PROVIDER_INVALID_FORMAT,
            mb_strimwidth(strval($provider), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified provider has valid format.
	 * 
     * @param mixed $provider
	 * @return boolean
	 * @throws static
     */
    public static function setCheck($provider)
    {
        // Init var
        $result =
            // Check valid type
            ($provider instanceof UserProfileAttrProvider);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static($provider);
        }
		
		// Return result
		return $result;
    }
	
	
	
}