<?php
/**
 * This class allows to define user profile attribute value entity factory class.
 * User profile attribute value entity factory allows to provide new user profile attribute value entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\user\attribute\value\model;

use liberty_code\model\entity\factory\fix\model\FixEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory;
use people_sdk\user_profile\attribute\provider\model\UserProfileAttrProvider;
use people_sdk\user_profile\user\attribute\value\library\ConstUserProfileAttrValue;
use people_sdk\user_profile\user\attribute\value\exception\UserProfileAttrProviderInvalidFormatException;
use people_sdk\user_profile\user\attribute\value\model\UserProfileAttrValueEntity;



/**
 * @method UserProfileAttrProvider getObjUserProfileAttrProvider() Get user profile attribute provider object.
 * @method void setObjUserProfileAttrProvider(UserProfileAttrProvider $objUserProfileAttrProvider) Set user profile attribute provider object.
 * @method UserProfileAttrValueEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 */
class UserProfileAttrValueEntityFactory extends FixEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************

	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param UserProfileAttrProvider $objUserProfileAttrProvider
     */
    public function __construct(
        ProviderInterface $objProvider,
        UserProfileAttrProvider $objUserProfileAttrProvider
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init user profile attribute provider
        $this->setObjUserProfileAttrProvider($objUserProfileAttrProvider);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstUserProfileAttrValue::DATA_KEY_DEFAULT_USER_PROFILE_ATTR_PROVIDER))
        {
            $this->__beanTabData[ConstUserProfileAttrValue::DATA_KEY_DEFAULT_USER_PROFILE_ATTR_PROVIDER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstUserProfileAttrValue::DATA_KEY_DEFAULT_USER_PROFILE_ATTR_PROVIDER,
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstUserProfileAttrValue::DATA_KEY_DEFAULT_USER_PROFILE_ATTR_PROVIDER:
                    UserProfileAttrProviderInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => UserProfileAttrValueEntity::class
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $objAttrProvider = $this->getObjUserProfileAttrProvider();
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $result = new UserProfileAttrValueEntity(
            $objAttrProvider,
            array(),
            $objValidator
        );

        // Return result
        return $result;
    }



}