<?php
/**
 * This class allows to define user profile attribute value entity class.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\user\attribute\value\model;

use liberty_code\handle_model\entity\repository\model\HandleSaveConfigEntity;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\handle_model\attribute\provider\api\AttrProviderInterface;
use people_sdk\user_profile\attribute\provider\model\UserProfileAttrProvider;



/**
 * @method UserProfileAttrProvider getObjAttrProvider() @inheritdoc
 */
class UserProfileAttrValueEntity extends HandleSaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param UserProfileAttrProvider $objAttrProvider
     */
    public function __construct(
        UserProfileAttrProvider $objAttrProvider,
        array $tabValue = array(),
        ValidatorInterface $objValidator = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objAttrProvider,
            $tabValue,
            $objValidator
        );
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setAttrProvider(AttrProviderInterface $objAttrProvider)
    {
        // Set data, if required
        if($objAttrProvider instanceof UserProfileAttrProvider)
        {
            // Call parent method
            parent::setAttrProvider($objAttrProvider);
        }
    }



}