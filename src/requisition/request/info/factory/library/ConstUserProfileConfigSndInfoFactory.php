<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\requisition\request\info\factory\library;



class ConstUserProfileConfigSndInfoFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_USER_PROFILE_SUPPORT_TYPE = 'user_profile_support_type';
    const TAB_CONFIG_KEY_USER_PROFILE_CURRENT_PROFILE_INCLUDE_CONFIG_KEY = 'user_profile_current_profile_include_config_key';
    const TAB_CONFIG_KEY_USER_PROFILE_ROLE_SUPPORT_TYPE = 'user_profile_role_support_type';
    const TAB_CONFIG_KEY_USER_PROFILE_ROLE_PERM_FULL_UPDATE_CONFIG_KEY = 'user_profile_role_perm_full_update_config_key';
    const TAB_CONFIG_KEY_USER_PROFILE_ROLE_ROLE_FULL_UPDATE_CONFIG_KEY = 'user_profile_role_role_full_update_config_key';

    // Support type configuration
    const CONFIG_SUPPORT_TYPE_URL_ARG = 'url_argument';
    const CONFIG_SUPPORT_TYPE_HEADER = 'header';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the user profile configuration sending information factory standard.';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get configuration array of support types.
     *
     * @return array
     */
    public static function getTabConfigSupportType()
    {
        // Init var
        $result = array(
            self::CONFIG_SUPPORT_TYPE_URL_ARG,
            self::CONFIG_SUPPORT_TYPE_HEADER
        );

        // Return result
        return $result;
    }



}