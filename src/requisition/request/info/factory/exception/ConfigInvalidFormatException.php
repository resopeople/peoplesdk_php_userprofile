<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\requisition\request\info\factory\exception;

use Exception;

use people_sdk\user_profile\requisition\request\info\factory\library\ConstUserProfileConfigSndInfoFactory;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstUserProfileConfigSndInfoFactory::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $tabConfigKey = array(
            ConstUserProfileConfigSndInfoFactory::TAB_CONFIG_KEY_USER_PROFILE_CURRENT_PROFILE_INCLUDE_CONFIG_KEY,
            ConstUserProfileConfigSndInfoFactory::TAB_CONFIG_KEY_USER_PROFILE_ROLE_PERM_FULL_UPDATE_CONFIG_KEY,
            ConstUserProfileConfigSndInfoFactory::TAB_CONFIG_KEY_USER_PROFILE_ROLE_ROLE_FULL_UPDATE_CONFIG_KEY
        );

        // Run each config
        $result = true;
        for($intCpt = 0; ($intCpt < count($tabConfigKey)) && $result; $intCpt++)
        {
            $strConfigKey = $tabConfigKey[$intCpt];
            $result =
                // Check valid config key
                (
                    (!isset($config[$strConfigKey])) ||
                    (
                        is_string($config[$strConfigKey]) &&
                        (trim($config[$strConfigKey]) != '')
                    )
                );
        };

        if($result)
        {
            // Init var
            $tabConfigKey = array(
                ConstUserProfileConfigSndInfoFactory::TAB_CONFIG_KEY_USER_PROFILE_SUPPORT_TYPE,
                ConstUserProfileConfigSndInfoFactory::TAB_CONFIG_KEY_USER_PROFILE_ROLE_SUPPORT_TYPE
            );

            // Run each config
            for($intCpt = 0; ($intCpt < count($tabConfigKey)) && $result; $intCpt++)
            {
                $strConfigKey = $tabConfigKey[$intCpt];
                $result =
                    // Check valid support type
                    (
                        (!isset($config[$strConfigKey])) ||
                        (
                            is_string($config[$strConfigKey]) &&
                            in_array(
                                $config[$strConfigKey],
                                ConstUserProfileConfigSndInfoFactory::getTabConfigSupportType()
                            )
                        )
                    );
            };
        }

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}