<?php
/**
 * Description :
 * This class allows to define user profile configuration sending information factory class.
 * User profile configuration sending information factory uses user profile values, from source configuration object,
 * to provide HTTP request sending information.
 *
 * User profile configuration sending information factory uses the following specified configuration:
 * [
 *     Default configuration sending information factory,
 *
 *     user_profile_support_type(optional: got "header", if not found):
 *         "string support type, to set user profile options parameters.
 *         Scope of available values: @see ConstUserProfileConfigSndInfoFactory::getTabConfigSupportType()",
 *
 *     user_profile_current_profile_include_config_key(optional):
 *         "string configuration key, to get user profile current profile include option,
 *         used on user profile options",
 *
 *     user_profile_role_support_type(optional: got "header", if not found):
 *         "string support type, to set user profile role options parameters.
 *         Scope of available values: @see ConstUserProfileConfigSndInfoFactory::getTabConfigSupportType()",
 *
 *     user_profile_role_perm_full_update_config_key(optional):
 *         "string configuration key, to get user profile role permission full update option
 *         used on user profile role options"
 *
 *     user_profile_role_role_full_update_config_key(optional):
 *         "string configuration key, to get user profile role role full update option
 *         used on user profile role options"
 * ]
 *
 * User profile configuration sending information factory uses the following values,
 * from specified source configuration object, to get sending information:
 * {
 *     Value <user_profile_current_profile_include_config_key>(optional): true / false,
 *
 *     Value <user_profile_role_perm_full_update_config_key>(optional): true / false,
 *
 *     Value <user_profile_role_role_full_update_config_key>(optional): true / false
 * }
 *
 * Note:
 * -> Configuration support type:
 *     - "url_argument":
 *         Data put on URL arguments array.
 *     - "header":
 *         Data put on headers array.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\user_profile\requisition\request\info\factory\model;

use people_sdk\library\requisition\request\info\factory\config\model\DefaultConfigSndInfoFactory;

use people_sdk\library\requisition\request\info\library\ToolBoxSndInfo;
use people_sdk\library\requisition\request\info\factory\library\ConstSndInfoFactory;
use people_sdk\user_profile\user\requisition\request\info\library\ConstUserProfileSndInfo;
use people_sdk\user_profile\requisition\request\info\factory\library\ConstUserProfileConfigSndInfoFactory;
use people_sdk\user_profile\requisition\request\info\factory\exception\ConfigInvalidFormatException;



class UserProfileConfigSndInfoFactory extends DefaultConfigSndInfoFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstSndInfoFactory::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
	// ******************************************************************************

    /**
     * Get specified support type.
     *
     * @param string $strConfigKey
     * @param string $strDefault = ConstUserProfileConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
     * @return string
     */
    protected function getStrSupportType(
        $strConfigKey,
        $strDefault = ConstUserProfileConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
    )
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strDefault = (is_string($strDefault) ? $strDefault : ConstUserProfileConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER);
        $result = (
            isset($tabConfig[$strConfigKey]) ?
                $tabConfig[$strConfigKey] :
                $strDefault
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured user profile current profile include option,
     * used on user profile options.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithUserProfileCurrentProfileInclude(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstUserProfileConfigSndInfoFactory::TAB_CONFIG_KEY_USER_PROFILE_CURRENT_PROFILE_INCLUDE_CONFIG_KEY]) ?
                $tabConfig[ConstUserProfileConfigSndInfoFactory::TAB_CONFIG_KEY_USER_PROFILE_CURRENT_PROFILE_INCLUDE_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstUserProfileConfigSndInfoFactory::TAB_CONFIG_KEY_USER_PROFILE_SUPPORT_TYPE) ==
                    ConstUserProfileConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithParam(
                    ((intval($value) != 0) ? 1 : 0),
                    ($boolOnHeaderRequired ? ConstUserProfileSndInfo::HEADER_KEY_CURRENT_PROFILE_INCLUDE : null),
                    ((!$boolOnHeaderRequired) ? ConstUserProfileSndInfo::URL_ARG_KEY_CURRENT_PROFILE_INCLUDE : null)
                );
            },
            $tabInfo,
            function($value){
                return (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($value) ||
                    is_int($value) ||
                    (is_string($value) && ctype_digit($value))
                );
            }
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured user profile role permission full update option,
     * used on user profile role options.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithUserProfileRolePermFullUpdate(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstUserProfileConfigSndInfoFactory::TAB_CONFIG_KEY_USER_PROFILE_ROLE_PERM_FULL_UPDATE_CONFIG_KEY]) ?
                $tabConfig[ConstUserProfileConfigSndInfoFactory::TAB_CONFIG_KEY_USER_PROFILE_ROLE_PERM_FULL_UPDATE_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstUserProfileConfigSndInfoFactory::TAB_CONFIG_KEY_USER_PROFILE_ROLE_SUPPORT_TYPE) ==
                    ConstUserProfileConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithParam(
                    ((intval($value) != 0) ? 1 : 0),
                    ($boolOnHeaderRequired ? ConstUserProfileSndInfo::HEADER_KEY_PERM_FULL_UPDATE : null),
                    ((!$boolOnHeaderRequired) ? ConstUserProfileSndInfo::URL_ARG_KEY_PERM_FULL_UPDATE : null)
                );
            },
            $tabInfo,
            function($value){
                return (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($value) ||
                    is_int($value) ||
                    (is_string($value) && ctype_digit($value))
                );
            }
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured user profile role role full update option,
     * used on user profile role options.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithUserProfileRoleRoleFullUpdate(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstUserProfileConfigSndInfoFactory::TAB_CONFIG_KEY_USER_PROFILE_ROLE_ROLE_FULL_UPDATE_CONFIG_KEY]) ?
                $tabConfig[ConstUserProfileConfigSndInfoFactory::TAB_CONFIG_KEY_USER_PROFILE_ROLE_ROLE_FULL_UPDATE_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstUserProfileConfigSndInfoFactory::TAB_CONFIG_KEY_USER_PROFILE_ROLE_SUPPORT_TYPE) ==
                    ConstUserProfileConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithParam(
                    ((intval($value) != 0) ? 1 : 0),
                    ($boolOnHeaderRequired ? ConstUserProfileSndInfo::HEADER_KEY_ROLE_FULL_UPDATE : null),
                    ((!$boolOnHeaderRequired) ? ConstUserProfileSndInfo::URL_ARG_KEY_ROLE_FULL_UPDATE : null)
                );
            },
            $tabInfo,
            function($value){
                return (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($value) ||
                    is_int($value) ||
                    (is_string($value) && ctype_digit($value))
                );
            }
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabSndInfoEngine()
    {
        // Init var
        $result = $this->getTabSndInfoWithUserProfileCurrentProfileInclude();
        $result = $this->getTabSndInfoWithUserProfileRolePermFullUpdate($result);
        $result = $this->getTabSndInfoWithUserProfileRoleRoleFullUpdate($result);

        // Return result
        return $result;
    }



}