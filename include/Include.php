<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/attribute/library/ConstUserProfileAttr.php');
include($strRootPath . '/src/attribute/library/ToolBoxUserProfileAttr.php');
include($strRootPath . '/src/attribute/library/ToolBoxUserProfileAttrRepository.php');
include($strRootPath . '/src/attribute/model/UserProfileAttrEntity.php');
include($strRootPath . '/src/attribute/model/UserProfileAttrEntityCollection.php');
include($strRootPath . '/src/attribute/model/UserProfileAttrEntityFactory.php');
include($strRootPath . '/src/attribute/model/repository/UserProfileAttrEntitySimpleRepository.php');
include($strRootPath . '/src/attribute/model/repository/UserProfileAttrEntitySimpleCollectionRepository.php');
include($strRootPath . '/src/attribute/model/repository/UserProfileAttrEntityMultiRepository.php');
include($strRootPath . '/src/attribute/model/repository/UserProfileAttrEntityMultiCollectionRepository.php');

include($strRootPath . '/src/attribute/browser/library/ConstUserProfileAttrBrowser.php');
include($strRootPath . '/src/attribute/browser/model/UserProfileAttrBrowserEntity.php');

include($strRootPath . '/src/attribute/provider/model/UserProfileAttrProvider.php');

include($strRootPath . '/src/user/requisition/request/info/library/ConstUserProfileSndInfo.php');
include($strRootPath . '/src/user/requisition/request/info/library/ToolBoxUserProfileSndInfo.php');

include($strRootPath . '/src/user/attribute/value/library/ConstUserProfileAttrValue.php');
include($strRootPath . '/src/user/attribute/value/exception/UserProfileAttrProviderInvalidFormatException.php');
include($strRootPath . '/src/user/attribute/value/model/UserProfileAttrValueEntity.php');
include($strRootPath . '/src/user/attribute/value/model/UserProfileAttrValueEntityFactory.php');

include($strRootPath . '/src/user/library/ConstUserProfile.php');
include($strRootPath . '/src/user/exception/UserProfileAttrValueEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/user/exception/UserProfileAttrValueEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/user/model/UserProfileEntity.php');
include($strRootPath . '/src/user/model/UserProfileEntityCollection.php');
include($strRootPath . '/src/user/model/UserProfileEntityFactory.php');
include($strRootPath . '/src/user/model/repository/UserProfileEntitySimpleRepository.php');
include($strRootPath . '/src/user/model/repository/UserProfileEntityMultiRepository.php');
include($strRootPath . '/src/user/model/repository/UserProfileEntityMultiCollectionRepository.php');

include($strRootPath . '/src/user/browser/library/ConstUserProfileBrowser.php');
include($strRootPath . '/src/user/browser/model/UserProfileBrowserEntity.php');

include($strRootPath . '/src/api/requisition/request/info/library/ToolBoxUserProfileApiKeySndInfo.php');

include($strRootPath . '/src/api/library/ConstUserProfileApiKey.php');
include($strRootPath . '/src/api/exception/UserProfileEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/api/exception/UserProfileEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/api/model/UserProfileApiKeyEntity.php');
include($strRootPath . '/src/api/model/UserProfileApiKeyEntityCollection.php');
include($strRootPath . '/src/api/model/UserProfileApiKeyEntityFactory.php');
include($strRootPath . '/src/api/model/repository/UserProfileApiKeyEntitySimpleRepository.php');
include($strRootPath . '/src/api/model/repository/UserProfileApiKeyEntityMultiRepository.php');
include($strRootPath . '/src/api/model/repository/UserProfileApiKeyEntityMultiCollectionRepository.php');

include($strRootPath . '/src/api/browser/library/ConstUserProfileApiKeyBrowser.php');
include($strRootPath . '/src/api/browser/model/UserProfileApiKeyBrowserEntity.php');

include($strRootPath . '/src/token/requisition/request/info/library/ToolBoxUserProfileTokenKeySndInfo.php');

include($strRootPath . '/src/token/library/ConstUserProfileTokenKey.php');
include($strRootPath . '/src/token/exception/UserProfileEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/token/exception/UserProfileEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/token/model/UserProfileTokenKeyEntity.php');
include($strRootPath . '/src/token/model/UserProfileTokenKeyEntityCollection.php');
include($strRootPath . '/src/token/model/UserProfileTokenKeyEntityFactory.php');
include($strRootPath . '/src/token/model/repository/UserProfileTokenKeyEntitySimpleRepository.php');
include($strRootPath . '/src/token/model/repository/UserProfileTokenKeyEntityMultiRepository.php');
include($strRootPath . '/src/token/model/repository/UserProfileTokenKeyEntityMultiCollectionRepository.php');

include($strRootPath . '/src/token/browser/library/ConstUserProfileTokenKeyBrowser.php');
include($strRootPath . '/src/token/browser/model/UserProfileTokenKeyBrowserEntity.php');

include($strRootPath . '/src/requisition/request/info/factory/library/ConstUserProfileConfigSndInfoFactory.php');
include($strRootPath . '/src/requisition/request/info/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/requisition/request/info/factory/model/UserProfileConfigSndInfoFactory.php');